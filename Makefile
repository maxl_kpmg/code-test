IMAGE_NAME ?= max-test
DOCKER_HUB_IMAGE_NAME ?= max-test

.PHONY: build
build:
	echo "\n----- Build Start -----\n" && \
	date && \
	npm i && \
	npm run build && \
	docker build -t $(IMAGE_NAME) ./ && \
	docker tag $(DOCKER_HUB_IMAGE_NAME) 13357200827/code:max-1 && \
	docker push 13357200827/code:max-1 && \
	date && \
	echo "\n----- Build Done -----\n"

.PHONY: start
start:
	echo "\n----- Start -----\n" && \
	date && \
	docker run -it -p 8080:8080 $(IMAGE_NAME)
	date && \
	echo "\n----- Done -----\n"